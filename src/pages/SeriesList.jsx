import React, { Component } from 'react'
import ReactTable from 'react-table-6'
import api from '../api'

import styled from 'styled-components'

import 'react-table-6/react-table.css'

const Wrapper = styled.div`
    padding: 0 40px 40px 40px;
`;

const Update = styled.div`
    color: #ef9b0f;
    cursor: pointer;
`;

const Delete = styled.div`
    color: #ff0000;
    cursor: pointer;
`;

class UpdateSerial extends Component {
    updateUser = event => {
        event.preventDefault();

        window.location.href = `/series/update/${this.props.id}`
    };

    render() {
        return <Update onClick={this.updateUser}>Update</Update>
    }
}

class DeleteSerial extends Component {
    deleteUser = event => {
        event.preventDefault();

        if (
            window.confirm(
                `Do tou want to delete the serial ${this.props.id} permanently?`,
            )
        ) {
            api.deleteSerialById(this.props.id);
            window.location.reload()
        }
    };
    render() {
        return <Delete onClick={this.deleteUser}>Delete</Delete>
    }
}

class SeriesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            series: [],
            columns: [],
            isLoading: false,
        }
    }

    componentDidMount = async () => {
        this.setState({ isLoading: true });

        await api.getAllSeries().then(series => {
            this.setState({
                series: series.data.data,
                isLoading: false,
            });
            console.log(`З апі прийшло ${series}`)
        })
    };



    render() {
        const { series, isLoading } = this.state;
        console.log('TCL: SeriesList -> render -> movies', series);

        const columns = [
            {
                Header: 'ID',
                accessor: 'id',
                filterable: true,
            },
            {
                Header: 'Name',
                accessor: 'name',
                filterable: true,
            },
            {
                Header: 'Rating',
                accessor: 'rating',
                filterable: true,
            },
            {
                Header: 'Time',
                accessor: 'time',
                Cell: props => <span>{props.value}</span>,
                // Cell: props => <span>{props.value.join(' / ')}</span>,  // Забрав join(' / ') бо була помилка
            },
            {
                Header: 'Sesons',
                accessor: 'sesons',
                filterable: true,
            },
            {
                Header: '',
                accessor: '',
                Cell: function(props) {
                    //  у MongoDB _id
                    return (
                        <span>
                            <DeleteSerial id={props.original.id} />
                        </span>
                    )
                },
            },
            {
                Header: '',
                accessor: '',
                Cell: function(props) {
                    return (
                        // у MongoDB _id
                        <span>
                            <UpdateSerial id={props.original.id} />
                        </span>
                    )
                },
            },
        ];

        let showTable = true;
        if (!series.length) {
            showTable = false
        }

        return (
            <Wrapper>
                {showTable && (
                    <ReactTable
                        data={series}
                        columns={columns}
                        loading={isLoading}
                        defaultPageSize={10}
                        showPageSizeOptions={true}
                        minRows={0}
                    />
                )}
            </Wrapper>
        )
    }
}

export default SeriesList