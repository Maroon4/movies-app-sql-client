import MoviesList from './MoviesList'
import MoviesInsert from './MoviesInsert'
import MoviesUpdate from './MoviesUpdate'

import LoginForm from "./LoginForm";
import SignUp from "./SignUp";

import SeriesList from "./SeriesList";
import SeriesInsert from "./SeriesInsert";
import SerialUpdate from "./SerialUpdate";

export { MoviesList,
         MoviesInsert,
         MoviesUpdate,
         LoginForm,
         SignUp,
         SeriesList,
         SeriesInsert,
         SerialUpdate
}