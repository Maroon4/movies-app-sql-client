import React, { Component } from 'react'
import api from '../api'

import styled from 'styled-components'

const Title = styled.h1.attrs({
    className: 'h1',
})``;

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`;

const Label = styled.label`
    margin: 5px;
`;

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`;

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`;

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`;

class SeriesInsert extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            rating: '',
            time: '',
            sesons: ''
        }
    }

    handleChangeInputName = async event => {
        const name = event.target.value;
        this.setState({ name })
    };

    handleChangeInputRating = async event => {
        const rating = event.target.validity.valid
            ? event.target.value
            : this.state.rating;

        this.setState({ rating })
    };

    handleChangeInputTime = async event => {
        const time = event.target.value;
        this.setState({ time })
    };

    handleChangeInputSesons = async event => {
        const sesons = event.target.value;
        this.setState( {sesons} )
    };

    handleIncludeSeries = async () => {
        const { name, time, rating, sesons} = this.state;
        // const arrayTime = time.split('/');
        const payload = { name,  time, rating, sesons };

        await api.insertSerial(payload).then(res => {

            console.log(res);

            if (res.status === 201) {
                window.alert('Series inserted successfully');
            }
            // this.setState( {
            //     name: '',
            //     rating: '',
            //     time: '',
            //     sesons: ''
            // })
        }, function (err) {
            if (err.response.status === 500) {
                window.alert(`Crearing series is failed`);
            }
            return Promise.reject(err.response)
        })

    };

    render() {
        const {name, time, rating, sesons} = this.state;

        return (
            <Wrapper>
                <Title>Create Serial</Title>

                <Label>Name: </Label>
                <InputText
                    type="text"
                    value={name}
                    onChange={this.handleChangeInputName}
                />

                <Label>Rating: </Label>
                <InputText
                    type="number"
                    step="0.1"
                    lang="en-US"
                    min="0"
                    max="10"
                    pattern="[0-9]+([,\.][0-9]+)?"
                    value={rating}
                    onChange={this.handleChangeInputRating}
                />

                <Label>Time: </Label>
                <InputText
                    type="text"
                    value={time}
                    onChange={this.handleChangeInputTime}
                />

                <Label>Sesons: </Label>
                <InputText
                    type="number"
                    value={sesons}
                    onChange={this.handleChangeInputSesons}
                />

                <Button onClick={this.handleIncludeSeries}>Add Serial</Button>
                <CancelButton href={'/series/list'}>Cancel</CancelButton>

            </Wrapper>
        );
    }

}

export  default SeriesInsert


