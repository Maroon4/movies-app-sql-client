import React, { Component } from 'react'
import api from '../api'

import styled from 'styled-components'

const Title = styled.h1.attrs({
    className: 'h1',
})``;

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`;

const Label = styled.label`
    margin: 5px;
`;

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`;

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`;

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`;

class SerialUpdate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            name: '',
            rating: '',
            time: '',
            sesons: ''
        }
    }

    handleChangeInputName = async event => {
        const name = event.target.value;
        this.setState({ name })
    };

    handleChangeInputRating = async event => {
        const rating = event.target.validity.valid
            ? event.target.value
            : this.state.rating;

        this.setState({ rating })
    };

    handleChangeInputTime = async event => {
        const time = event.target.value;
        this.setState({ time })
    };

    handleChangeInputSesons = async event => {
        const sesons = event.target.value;
        this.setState({ sesons })
    };

    handleUpdateSerial = async () => {
        const { id, name, rating, time, sesons } = this.state;
        const payload = { name, rating, time, sesons};

        await api.updateSeriesById(id, payload).then(res => {
             if (res.status === 200) {
                 window.alert(`Serial updated successfully`);
                 this.setState({
                     name: '',
                     rating: '',
                     time: '',
                     sesons: ''
                 })
             }
        }, function (err) {
            if (err.response.status === 400) {
                window.alert(`Wrong data inserted`);
            }
            if (err.response.status === 404) {
                window.alert(`Serial not updated`);
            }
            return Promise.reject(err.response)
        })
    };

    componentDidMount = async () => {
        const { id } = this.state;

        console.log(`буде змінено ${id}`);

        const serial = await api.getSerialById(id);

        console.log(`Data of serial ${serial.data.data.name}`);

        this.setState({
            name: serial.data.data.name,
            rating: serial.data.data.rating,
            time: serial.data.data.time,
            sesons: serial.data.data.sesons
            // time: serial.data.data.time.join('/'),  // Забрав join(' / ') бо була помилка
        })
    };

    render() {
        const { name, rating, time, sesons} =this.state;

        return (
            <Wrapper>
                <Title>Update Serial</Title>

                <Label>Name: </Label>
                <InputText
                    type="text"
                    value={name}
                    onChange={this.handleChangeInputName}
                />

                <Label>Rating: </Label>
                <InputText
                    type="number"
                    step="0.1"
                    lang="en-US"
                    min="0"
                    max="10"
                    pattern="[0-9]+([,\.][0-9]+)?"
                    value={rating}
                    onChange={this.handleChangeInputRating}
                />

                <Label>Time: </Label>
                <InputText
                    type="text"
                    value={time}
                    onChange={this.handleChangeInputTime}
                />

                <Label>Sesons: </Label>
                <InputText
                    type="number"
                    value={sesons}
                    onChange={this.handleChangeInputSesons}
                />
                <Button
                    // href={'/series/list'}
                    onClick={this.handleUpdateSerial}
                >Update Serial</Button>
                <CancelButton href={'/series/list'}>Cancel</CancelButton>
            </Wrapper>
        );
    }
}

export default SerialUpdate


