import axios from 'axios'

const api = axios.create({
    baseURL: 'http://localhost:3000/api',
});

export const insertMovie = payload => api.post(`/movie`, payload);
export const getAllMovies = () => api.get(`/movies`);
export const updateMovieById = (id, payload) => api.put(`/movie/${id}`, payload);
export const deleteMovieById = id => api.delete(`/movie/${id}`);
export const getMovieById = id => api.get(`/movie/${id}`);

export const loginUser = payload => api.post(`/auth/signin`, payload);
export const signUp = payload => api.post(`/auth/signup`, payload);

export const insertSerial = payload => api.post(`/serial`, payload);
export const getAllSeries = () => api.get(`/series`);
export const updateSeriesById = (id, payload) => api.put(`/serial/${id}`);
export const deleteSerialById = id => api.delete(`/serial/${id}`);
export const getSerialById = id => api.get(`/serial/${id}`);



const apis = {
    insertMovie,
    getAllMovies,
    updateMovieById,
    deleteMovieById,
    getMovieById,
    loginUser,
    signUp,
    insertSerial,
    getAllSeries,
    updateSeriesById,
    deleteSerialById,
    getSerialById
};

export default apis